package com.example.intents;

import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView tv_name;
    TextView tv_surname;
    TextView tv_web;

    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        prefs = getSharedPreferences("MyPreferences", MODE_PRIVATE);

        int counter = prefs.getInt("counter", 0);
        counter = counter + 1;

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("counter", counter);
        editor.commit();


        tv_name = findViewById(R.id.tv_name);
        tv_surname = findViewById(R.id.et_surname);
        tv_web = findViewById(R.id.et_web);
        tv_number = findViewById(R.id.et_number);

        String name = getIntent().getStringExtra("name");
        String surname = getIntent().getStringExtra("surname");
        String web = getIntent().getStringExtra("web");
        int number = getIntent().getIntExtra("number", -1);

        //tv_name.setText(name + " " + number);



    }

    public void closePressed(View view) {
        finish();
    }
}
