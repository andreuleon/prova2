package com.example.intents;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    EditText et_name, et_surname, et_web, et_number;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_name = findViewById(R.id.et_name);
        et_surname = findViewById(R.id.et_surname);
        et_web = findViewById(R.id.et_web);
        et_number = findViewById(R.id.et_number);

        prefs = getSharedPreferences("MyPreferences", MODE_PRIVATE);

    }

    public void goToSecondActivity(View view) {
        //Recollir el continut del EditText:
        String name = et_name.getText().toString();
        String surname = et_surname.getText().toString();
        String web = et_web.getText().toString();
        int number = Integer.parseInt(et_number.getText().toString());
        Intent intent = new Intent(MainActivity.this,
                SecondActivity.class);
        //L'envio com a Extra a l'altre activity:
        intent.putExtra("surname", surname);
        //intent.putExtra("text",text);
        intent.putExtra("web", web);
        intent.putExtra("number", number);
        startActivity(intent);


    }

    public void saveShared(View view) {

        SharedPreferences.Editor edit = prefs.edit();
        edit.putString("valor",et_name.getText().toString());
        edit.putString("valor", et_surname.getText().toString());
        edit.putString("valor", et_web.getText().toString());
        edit.commit();


    }

    public void showShared(View view) {
        Toast.makeText(this,
                String.valueOf(prefs.getInt("counter",0)),
                Toast.LENGTH_LONG).show();
    }
}
