package com.example.intents;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText et_text, et_number;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_text = findViewById(R.id.et_text);
        et_number = findViewById(R.id.et_number);
    }

    public void goToSecondActivity(View view) {
        //Recollir el continut del EditText:
        String text = et_text.getText().toString();
        int number = Integer.parseInt(et_number.getText().toString());
        Intent intent = new Intent(MainActivity.this,
                SecondActivity.class);
        //L'envio com a Extra a l'altre activity:
        intent.putExtra("text",text);
        intent.putExtra("number", number);
        startActivity(intent);
    }

    public void showShared (view view) {
        Toast.makeText()
    }


}
