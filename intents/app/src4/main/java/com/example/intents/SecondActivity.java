package com.example.intents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView tv_name;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        prefs = getSharedPreferences("MyPreferences", MODE_PRIVATE);

        int counter = prefs.getInt("counter", 0);
        counter = counter + 1;

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("counter", counter);
        editor.commit();

        tv_name = findViewById(R.id.tv_name);

        String text = getIntent().getStringExtra("text");
        int number = getIntent().getIntExtra("number",-1);

        tv_name.setText(text+" "+number);
    }

    public void closePressed(View view) {
        finish();
    }
}
