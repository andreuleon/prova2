package com.example.intents;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView tv_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tv_name = findViewById(R.id.tv_name);

        String name = getIntent().getStringExtra("name");
        int age = getIntent().getIntExtra("age",0);

        tv_name.setText(name +" "+ age);
    }

    public void closePressed(View view) {
        finish();
    }
}
